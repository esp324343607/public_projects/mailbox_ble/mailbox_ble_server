#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>

static BLEUUID serviceUUID("54d7df80-11fe-11ee-be56-0242ac120002");
static const uint8_t LED_PIN = 2;
static const uint16_t AWAKE_DURATION = 2000;
static unsigned long awakeStartTime;
static bool debug = false;

void setUpBLE() {
    BLEDevice::init("BLE_SERVER_MAILBOX");
    BLEServer* server = BLEDevice::createServer();
    BLEService* service = server->createService(serviceUUID);
    BLEAdvertising* advertising = BLEDevice::getAdvertising();
    service->start();
    advertising->addServiceUUID(serviceUUID);
    advertising->start();
    awakeStartTime = millis();
    if (debug) Serial.println(F("(B) Advertising ..."));
    digitalWrite(LED_PIN, HIGH);
}

void setup() {
    Serial.begin(115200);
    pinMode(LED_PIN, OUTPUT);
    esp_deep_sleep_enable_gpio_wakeup(1ULL << 3, ESP_GPIO_WAKEUP_GPIO_HIGH);
    if (debug) Serial.println(F("\n(A) BLE server application started."));
    if (esp_sleep_get_wakeup_cause() == 7) {
        setUpBLE();
    }
}

void loop() {
    if (millis() - awakeStartTime > AWAKE_DURATION) {
        if (debug) Serial.println(F("(Z) Going to sleep (disconnecting)."));
        digitalWrite(LED_PIN, LOW);
        esp_deep_sleep_start();
    }
}
