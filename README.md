# Mailbox BLE Server

![Mailbox BLE Server schematic](images/mailbox-ble-server-schematic.png)

Gerber files for PCB fabrication are available for inspection, download, and ordering at [OSHPark](https://oshpark.com/shared_projects/yjYZCtoY).
